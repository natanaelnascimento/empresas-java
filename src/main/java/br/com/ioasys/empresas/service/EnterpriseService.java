package br.com.ioasys.empresas.service;

import br.com.ioasys.empresas.model.EnterpriseEntity;

public interface EnterpriseService extends EntityService<EnterpriseEntity> {

}
