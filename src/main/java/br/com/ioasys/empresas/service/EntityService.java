package br.com.ioasys.empresas.service;

import java.util.Optional;

import br.com.ioasys.empresas.model.GenericEntity;

public interface EntityService<T extends GenericEntity> {

	public Optional<T> findById(Long id);

	public Iterable<T> findAll();

	public Iterable<T> findAll(T entity);

	public T save(T entity);

	public void delete(T entity);

}
