package br.com.ioasys.empresas.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import br.com.ioasys.empresas.model.EnterpriseTypeEntity;
import br.com.ioasys.empresas.repository.EnterpriseTypeRepository;

@Service
@Transactional
public class EnterpriseTypeServiceImpl implements EnterpriseTypeService {

	@Autowired
	private EnterpriseTypeRepository enterpriseTypeRepository;

	@Override
	public Optional<EnterpriseTypeEntity> findById(Long id) {
		return enterpriseTypeRepository.findById(id);
	}

	@Override
	public Iterable<EnterpriseTypeEntity> findAll() {
		return enterpriseTypeRepository.findAll();
	}

	@Override
	public Iterable<EnterpriseTypeEntity> findAll(EnterpriseTypeEntity entity) {
		return enterpriseTypeRepository.findAll(Example.of(entity));
	}

	@Override
	public EnterpriseTypeEntity save(EnterpriseTypeEntity entity) {
		return enterpriseTypeRepository.save(entity);
	}

	@Override
	public void delete(EnterpriseTypeEntity entity) {
		enterpriseTypeRepository.delete(entity);
	}

}
