package br.com.ioasys.empresas.service;

import java.util.Collections;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.ioasys.empresas.model.InvestorEntity;
import br.com.ioasys.empresas.pojo.ApplicationUserDetails;
import br.com.ioasys.empresas.repository.InvestorRepository;

@Service
@Transactional
public class InvestorServiceImpl implements InvestorService, UserDetailsService {

	@Autowired
	private InvestorRepository investorRepository;

	@Override
	public Optional<InvestorEntity> findById(Long id) {
		return investorRepository.findById(id);
	}

	@Override
	public Iterable<InvestorEntity> findAll() {
		return investorRepository.findAll();
	}

	@Override
	public Iterable<InvestorEntity> findAll(InvestorEntity entity) {
		return investorRepository.findAll(Example.of(entity));
	}

	@Override
	public Optional<InvestorEntity> findByEmail(String email) {
		return investorRepository.findByEmailIgnoreCase(email);
	}

	@Override
	public InvestorEntity save(InvestorEntity entity) {
		return investorRepository.save(entity);
	}

	@Override
	public void delete(InvestorEntity entity) {
		investorRepository.delete(entity);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		InvestorEntity user = findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("User not found."));
		ApplicationUserDetails applicationUserDetails = new ApplicationUserDetails();
		applicationUserDetails.setUsername(user.getEmail());
		applicationUserDetails.setPassword(user.getPassword());
		applicationUserDetails.setAccountNonExpired(true);
		applicationUserDetails.setAccountNonLocked(true);
		applicationUserDetails.setCredentialsNonExpired(true);
		applicationUserDetails.setEnabled(true);
		applicationUserDetails.setAuthorities(Collections.emptyList());
		
		return applicationUserDetails;
	}

}
