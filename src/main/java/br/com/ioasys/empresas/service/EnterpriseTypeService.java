package br.com.ioasys.empresas.service;

import br.com.ioasys.empresas.model.EnterpriseTypeEntity;

public interface EnterpriseTypeService extends EntityService<EnterpriseTypeEntity> {

}
