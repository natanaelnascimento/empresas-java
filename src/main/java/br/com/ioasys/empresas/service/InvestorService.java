package br.com.ioasys.empresas.service;

import java.util.Optional;

import br.com.ioasys.empresas.model.InvestorEntity;

public interface InvestorService extends EntityService<InvestorEntity> {

	public Optional<InvestorEntity> findByEmail(String email);

}
