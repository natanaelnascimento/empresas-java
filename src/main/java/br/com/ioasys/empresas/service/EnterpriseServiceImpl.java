package br.com.ioasys.empresas.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import br.com.ioasys.empresas.model.EnterpriseEntity;
import br.com.ioasys.empresas.repository.EnterpriseRepository;

@Service
@Transactional
public class EnterpriseServiceImpl implements EnterpriseService {

	@Autowired
	private EnterpriseRepository enterpriseRepository;

	@Override
	public Optional<EnterpriseEntity> findById(Long id) {
		return enterpriseRepository.findById(id);
	}

	@Override
	public Iterable<EnterpriseEntity> findAll() {
		return enterpriseRepository.findAll();
	}

	@Override
	public Iterable<EnterpriseEntity> findAll(EnterpriseEntity entity) {
		return enterpriseRepository.findAll(Example.of(entity));
	}

	@Override
	public EnterpriseEntity save(EnterpriseEntity entity) {
		return enterpriseRepository.save(entity);
	}

	@Override
	public void delete(EnterpriseEntity entity) {
		enterpriseRepository.delete(entity);
	}

}
