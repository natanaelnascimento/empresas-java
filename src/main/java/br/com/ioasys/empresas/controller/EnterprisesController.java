package br.com.ioasys.empresas.controller;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.empresas.model.EnterpriseEntity;
import br.com.ioasys.empresas.model.EnterpriseTypeEntity;
import br.com.ioasys.empresas.pojo.EnterpriseListResponse;
import br.com.ioasys.empresas.pojo.EnterpriseResponse;
import br.com.ioasys.empresas.service.EnterpriseService;

@RestController
@RequestMapping("/api/v1/enterprises")
public class EnterprisesController {

	@Autowired
	private EnterpriseService enterpriseService;

	@GetMapping("/{id}")
	public ResponseEntity<EnterpriseResponse> findById(@PathVariable Long id) {
		Optional<EnterpriseEntity> enterpriseResult = enterpriseService.findById(id);
		if(enterpriseResult.isPresent())
			return ResponseEntity.ok(EnterpriseResponse.fromEnterpriseEntity(enterpriseResult.get()));
		throw new EntityNotFoundException();
	}

	@GetMapping
	public ResponseEntity<EnterpriseListResponse> findByNameAndType(@RequestParam(name = "name", required = false) String name, @RequestParam(name = "enterprise_type", required = false) Long enterpriseTypeId) {
		EnterpriseEntity enterprise = new EnterpriseEntity();
		if(name != null)
			enterprise.setName(name);
		if(enterpriseTypeId != null) {
			EnterpriseTypeEntity enterpriseType = new EnterpriseTypeEntity();
			enterpriseType.setId(enterpriseTypeId);
			enterprise.setEnterpriseType(enterpriseType);
		}
		return ResponseEntity.ok(EnterpriseListResponse.fromEnterpriseEntityIterable(enterpriseService.findAll(enterprise)));
	}

}
