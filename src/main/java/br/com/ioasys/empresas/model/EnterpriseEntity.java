package br.com.ioasys.empresas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "enterprise")
public class EnterpriseEntity extends GenericEntity {

	@Column(nullable = false)
	private String name;

	private String description;
	private String email;
	private String facebook;
	private String twitter;
	private String linkedin;
	private String phone;
	private Boolean ownEnterprise;
	private String photo;
	private Integer value;
	private Integer shares;
	private Double sharePrice;
	private Integer ownShares;
	private String city;
	private String country;

	@ManyToOne(fetch = FetchType.EAGER)
	private EnterpriseTypeEntity enterpriseType;

}
