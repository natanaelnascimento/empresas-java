package br.com.ioasys.empresas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "enterprise_type")
public class EnterpriseTypeEntity extends GenericEntity {

	@Column(nullable = false)
	private String enterpriseTypeName;

}
