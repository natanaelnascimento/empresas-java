package br.com.ioasys.empresas.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "investor")
public class InvestorEntity extends GenericEntity {

	@Column(nullable = false)
	private String investorName;

	@Column(nullable = false)
	private String email;

	@Column(nullable = false)
	private String password;

	private String city;
	private String country;
	private Double balance;
	private String photo;
	private Double portfolioValue;
	private Boolean superAngel;
	private LocalDate lastAccess;

	@ManyToMany(fetch = FetchType.EAGER)
	private List<EnterpriseEntity> portfolioEnterprises;

}
