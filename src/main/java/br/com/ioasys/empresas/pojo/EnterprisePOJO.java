package br.com.ioasys.empresas.pojo;

import br.com.ioasys.empresas.model.EnterpriseEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class EnterprisePOJO extends EntityPOJO {

	private static final long serialVersionUID = 1L;

	private String name;
	private String description;
	private String email;
	private String facebook;
	private String twitter;
	private String linkedin;
	private String phone;
	private Boolean ownEnterprise;
	private String photo;
	private Integer value;
	private Integer shares;
	private Double sharePrice;
	private Integer ownShares;
	private String city;
	private String country;
	private EnterpriseTypePOJO enterpriseType;

	public static EnterprisePOJO fromEnterpriseEntity(EnterpriseEntity enterprise) {
		if(enterprise == null)
			return null;
		EnterprisePOJO pojo = new EnterprisePOJO();
		pojo.setId(enterprise.getId());
		pojo.setName(enterprise.getName());
		pojo.setDescription(enterprise.getDescription());
		pojo.setEmail(enterprise.getEmail());
		pojo.setFacebook(enterprise.getFacebook());
		pojo.setTwitter(enterprise.getTwitter());
		pojo.setLinkedin(enterprise.getLinkedin());
		pojo.setPhone(enterprise.getPhone());
		pojo.setOwnEnterprise(enterprise.getOwnEnterprise());
		pojo.setPhoto(enterprise.getPhoto());
		pojo.setValue(enterprise.getValue());
		pojo.setShares(enterprise.getShares());
		pojo.setSharePrice(enterprise.getSharePrice());
		pojo.setOwnShares(enterprise.getOwnShares());
		pojo.setCity(enterprise.getCity());
		pojo.setCountry(enterprise.getCountry());
		pojo.setEnterpriseType(EnterpriseTypePOJO.fromEnterpriseTypeEntity(enterprise.getEnterpriseType()));
		return pojo;
	}

}
