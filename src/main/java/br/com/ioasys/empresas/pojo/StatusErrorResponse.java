package br.com.ioasys.empresas.pojo;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class StatusErrorResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private String status;
	private String error;

	public static StatusErrorResponse fromHttpStatus(HttpStatus status) {
		StatusErrorResponse response = new StatusErrorResponse();
		response.setStatus(String.valueOf(status.value()));
		response.setError(status.getReasonPhrase());
		return response;
	}

}
