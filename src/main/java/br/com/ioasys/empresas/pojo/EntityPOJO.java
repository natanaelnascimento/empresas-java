package br.com.ioasys.empresas.pojo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class EntityPOJO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

}
