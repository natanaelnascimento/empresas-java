package br.com.ioasys.empresas.pojo;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@EqualsAndHashCode
public class AccessDeniedErrorResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private final boolean success = false;

	@Setter
	private List<String> errors;

	public static AccessDeniedErrorResponse of(String... errors) {
		AccessDeniedErrorResponse response = new AccessDeniedErrorResponse();
		response.setErrors(Arrays.asList(errors));
		return response;
	}

}
