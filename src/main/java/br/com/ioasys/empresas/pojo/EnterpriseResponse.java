package br.com.ioasys.empresas.pojo;

import java.io.Serializable;

import br.com.ioasys.empresas.model.EnterpriseEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@EqualsAndHashCode
public class EnterpriseResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private final boolean success = true;

	@Setter
	private EnterprisePOJO enterprise;

	public static EnterpriseResponse fromEnterpriseEntity(EnterpriseEntity enterprise) {
		EnterpriseResponse response = new EnterpriseResponse();
		response.setEnterprise(EnterprisePOJO.fromEnterpriseEntity(enterprise));
		return response;
	}

}
