package br.com.ioasys.empresas.pojo;

import br.com.ioasys.empresas.model.InvestorEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class InvestorPOJO extends EntityPOJO {

	private static final long serialVersionUID = 1L;

	private String investorName;
	private String email;
	private String city;
	private String country;
	private Double balance;
	private String photo;
	private Double portfolioValue;
	private Boolean superAngel;
	private Boolean firstAccess;
	private PortfolioPOJO portfolio;

	public static InvestorPOJO fromInvestorEntity(InvestorEntity investor) {
		if(investor == null)
			return null;
		InvestorPOJO pojo = new InvestorPOJO();
		pojo.setId(investor.getId());
		pojo.setInvestorName(investor.getInvestorName());
		pojo.setEmail(investor.getEmail());
		pojo.setCity(investor.getCity());
		pojo.setCountry(investor.getCountry());
		pojo.setBalance(investor.getBalance());
		pojo.setPhoto(investor.getPhoto());
		pojo.setPortfolioValue(investor.getPortfolioValue());
		pojo.setSuperAngel(investor.getSuperAngel());
		pojo.setFirstAccess(investor.getLastAccess() == null);
		pojo.setPortfolio(PortfolioPOJO.fromEnterprisesEntityList(investor.getPortfolioEnterprises()));
		return pojo;
	}

}
