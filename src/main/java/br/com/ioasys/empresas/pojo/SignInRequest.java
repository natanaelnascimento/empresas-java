package br.com.ioasys.empresas.pojo;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class SignInRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String email;
	private String password;

}
