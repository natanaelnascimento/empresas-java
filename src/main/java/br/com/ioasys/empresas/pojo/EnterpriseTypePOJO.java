package br.com.ioasys.empresas.pojo;

import br.com.ioasys.empresas.model.EnterpriseTypeEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class EnterpriseTypePOJO extends EntityPOJO {

	private static final long serialVersionUID = 1L;

	private String enterpriseTypeName;

	public static EnterpriseTypePOJO fromEnterpriseTypeEntity(EnterpriseTypeEntity enterpriseType) {
		if(enterpriseType == null)
			return null;
		EnterpriseTypePOJO pojo = new EnterpriseTypePOJO();
		pojo.setId(enterpriseType.getId());
		pojo.setEnterpriseTypeName(enterpriseType.getEnterpriseTypeName());
		return pojo;
	}

}
