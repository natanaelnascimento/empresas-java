package br.com.ioasys.empresas.pojo;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@EqualsAndHashCode
public class AuthenticationErrorResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@Setter
	private List<String> errors;

	public static AuthenticationErrorResponse of(String... errors) {
		AuthenticationErrorResponse response = new AuthenticationErrorResponse();
		response.setErrors(Arrays.asList(errors));
		return response;
	}

}
