package br.com.ioasys.empresas.pojo;

import java.io.Serializable;

import br.com.ioasys.empresas.model.EnterpriseEntity;
import br.com.ioasys.empresas.model.InvestorEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@EqualsAndHashCode
public class SignInResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private final boolean success = true;

	@Setter
	private InvestorPOJO investor;

	@Setter
	private EnterprisePOJO enterprise;

	public static SignInResponse fromInvestorAndEnterpriseEntities(InvestorEntity investor, EnterpriseEntity enterprise) {
		SignInResponse response = new SignInResponse();
		response.setInvestor(InvestorPOJO.fromInvestorEntity(investor));
		response.setEnterprise(EnterprisePOJO.fromEnterpriseEntity(enterprise));
		return response;
	}

	public static SignInResponse fromInvestorEntity(InvestorEntity investor) {
		SignInResponse response = new SignInResponse();
		response.setInvestor(InvestorPOJO.fromInvestorEntity(investor));
		return response;
	}

	public static SignInResponse fromEnterpriseEntity(EnterpriseEntity enterprise) {
		SignInResponse response = new SignInResponse();
		response.setEnterprise(EnterprisePOJO.fromEnterpriseEntity(enterprise));
		return response;
	}

}
