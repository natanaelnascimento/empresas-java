package br.com.ioasys.empresas.pojo;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import br.com.ioasys.empresas.model.EnterpriseEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@EqualsAndHashCode
public class EnterpriseListResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@Setter
	private List<EnterprisePOJO> enterprises;

	public static EnterpriseListResponse fromEnterpriseEntityIterable(Iterable<EnterpriseEntity> enterprises) {
		EnterpriseListResponse response = new EnterpriseListResponse();
		if(enterprises == null)
			enterprises = Collections.emptyList();
		response.setEnterprises(StreamSupport.stream(enterprises.spliterator(), false)
				.map(EnterprisePOJO::fromEnterpriseEntity).collect(Collectors.toList()));
		return response;
	}

}
