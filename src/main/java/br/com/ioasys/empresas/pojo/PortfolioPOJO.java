package br.com.ioasys.empresas.pojo;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import br.com.ioasys.empresas.model.EnterpriseEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class PortfolioPOJO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer enterprisesNumber;
	private List<EnterprisePOJO> enterprises;

	public static PortfolioPOJO fromEnterprisesEntityList(List<EnterpriseEntity> enterprises) {
		if(enterprises == null)
			enterprises = Collections.emptyList();
		PortfolioPOJO pojo = new PortfolioPOJO();
		pojo.setEnterprisesNumber(enterprises.size());
		pojo.setEnterprises(enterprises.stream()
				.map(EnterprisePOJO::fromEnterpriseEntity)
				.collect(Collectors.toList()));
		return pojo;
	}

}
