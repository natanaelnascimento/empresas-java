package br.com.ioasys.empresas.repository;

import org.springframework.stereotype.Repository;

import br.com.ioasys.empresas.model.EnterpriseTypeEntity;

@Repository
public interface EnterpriseTypeRepository extends EntityRepository<EnterpriseTypeEntity> {

}
