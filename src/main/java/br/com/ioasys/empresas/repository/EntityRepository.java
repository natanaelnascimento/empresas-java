package br.com.ioasys.empresas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ioasys.empresas.model.GenericEntity;

public interface EntityRepository<T extends GenericEntity> extends JpaRepository<T, Long>{

}
