package br.com.ioasys.empresas.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import br.com.ioasys.empresas.model.InvestorEntity;

@Repository
public interface InvestorRepository extends EntityRepository<InvestorEntity> {

	public Optional<InvestorEntity> findByEmailIgnoreCaseAndPassword(String email, String password);

	public Optional<InvestorEntity> findByEmailIgnoreCase(String email);

}
