package br.com.ioasys.empresas.repository;

import org.springframework.stereotype.Repository;

import br.com.ioasys.empresas.model.EnterpriseEntity;

@Repository
public interface EnterpriseRepository extends EntityRepository<EnterpriseEntity> {

}
