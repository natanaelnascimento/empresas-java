package br.com.ioasys.empresas.security;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.ioasys.empresas.model.InvestorEntity;
import br.com.ioasys.empresas.pojo.AccessDeniedErrorResponse;
import br.com.ioasys.empresas.pojo.SignInRequest;
import br.com.ioasys.empresas.pojo.SignInResponse;
import br.com.ioasys.empresas.service.InvestorService;

public class JwtLoginFilter extends AbstractAuthenticationProcessingFilter {
	
	@Autowired
	private JwtAuthenticator jwtAuthenticator;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private InvestorService investorService;

	@Autowired
	private ObjectMapper objectMapper;

	protected JwtLoginFilter(String url, AuthenticationManager authManager) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(authManager);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
		SignInRequest signInRquest = objectMapper.readValue(request.getInputStream(), SignInRequest.class);
		return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(signInRquest.getEmail(), signInRquest.getPassword(), Collections.emptyList()));
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain, Authentication authentication) throws IOException, ServletException {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		jwtAuthenticator.setAuthentication(response, authentication);
		InvestorEntity investor = investorService.findByEmail(userDetails.getUsername()).get();
		investor.setLastAccess(LocalDate.now());
		investorService.save(investor);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.setStatus(HttpStatus.OK.value());
		objectMapper.writeValue(response.getOutputStream(), SignInResponse.fromInvestorEntity(investor));
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		AccessDeniedErrorResponse error = AccessDeniedErrorResponse.of("Invalid login credentials. Please try again.");
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		objectMapper.writeValue(response.getOutputStream(), error);
	}

}
