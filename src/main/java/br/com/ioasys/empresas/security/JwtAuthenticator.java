package br.com.ioasys.empresas.security;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtAuthenticator {

	@Autowired
	private UserDetailsService userDetailsService;

	@Value("${jwt.expiration}")
	private int jwtExpiration;

	@Value("${jwt.secret}")
	private String jwtSecret;

	@Value("${jwt.token-header}")
	private String jwtTokenHeader;

	@Value("${jwt.uid-header}")
	private String jwtUidHeader;

	@Value("${jwt.client-header}")
	private String jwtClientHeader;

	public Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response) {
		String token = request.getHeader(jwtTokenHeader);
		String uid = request.getHeader(jwtUidHeader);
		String client = request.getHeader(jwtClientHeader);
		try {
			Jwts.parser()
				.setSigningKey(jwtSecret)
				.require(jwtUidHeader, uid)
				.require(jwtClientHeader, client)
				.parseClaimsJws(token);
			UserDetails userDetails = userDetailsService.loadUserByUsername(uid);
			UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, Collections.emptyList());
			authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
			return authentication;
		} catch (Exception e) {
			return null;
		}
	}

	public void setAuthentication(HttpServletRequest request, HttpServletResponse response) {
		String uid = request.getHeader(jwtUidHeader);
		String client = request.getHeader(jwtClientHeader);
		String token = request.getHeader(jwtTokenHeader);
		response.addHeader(jwtTokenHeader, token);
		response.addHeader(jwtUidHeader, uid);
		response.addHeader(jwtClientHeader, client);
	}

	public void setAuthentication(HttpServletResponse response, Authentication authentication) {
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		String uid = userDetails.getUsername();
		String client = UUID.randomUUID().toString();
		Map<String, Object> claims = new HashMap<>();
		claims.put(jwtUidHeader, uid);
		claims.put(jwtClientHeader, client);
		String token = Jwts.builder()
				.setClaims(claims)
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + jwtExpiration))
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
		response.addHeader(jwtTokenHeader, token);
		response.addHeader(jwtUidHeader, uid);
		response.addHeader(jwtClientHeader, client);
	}

}
