DELETE FROM enterprise;
DELETE FROM enterprise_type;
DELETE FROM investor;
DELETE FROM investor_portfolio_enterprises;

INSERT INTO enterprise_type VALUES(1, 'Teste 1');
INSERT INTO enterprise_type VALUES(2, 'Teste 2');
INSERT INTO enterprise_type VALUES(3, 'Teste 3');

INSERT INTO enterprise VALUES(1, 'Aracaju', 'Brasil', 'Descrição de teste', null, null, null, 'Empresa teste 1', false, 0, null,  null, 500.0, 100, null, 0, 1);
INSERT INTO enterprise VALUES(2, 'Aracaju', 'Brasil', 'Descrição de teste', null, null, null, 'Empresa teste 2', false, 0, null,  null, 600.0, 200, null, 0, 2);
INSERT INTO enterprise VALUES(3, 'Aracaju', 'Brasil', 'Descrição de teste', null, null, null, 'Empresa teste 3', false, 0, null,  null, 800.0, 300, null, 0, 3);

INSERT INTO investor VALUES(1, 50.00, 'Aracaju', 'Brasil', 'teste1@teste.com', 'Teste 1', null, '$2y$10$cSpvwaudH4e64b3LuKO8F.cNJPqLUZlXJL.lr9zALb9HeAFyOlRP2', null, 50.00, false);
INSERT INTO investor VALUES(2, 350.00, 'Aracaju', 'Brasil', 'teste2@teste.com', 'Teste 2', null, '$2y$10$uAjnKTdNbyyZ7DyJgQV0weMBP67495ONb/xd65H38m7zftZsJwYam', null, 350.00, false);
INSERT INTO investor VALUES(3, 6000.00, 'Aracaju', 'Brasil', 'teste3@teste.com', 'Teste 3', null, '$2y$10$t9x.TMdJstgf9hYvZ39WRuhfMyeVIGgP9J2g5jFrzyLJtr4g57Tva', null, 6000.00, false);
INSERT INTO investor VALUES(4, 50000.00, 'Aracaju', 'Brasil', 'testeapple@ioasys.com.br', 'Teste Apple', null, '$2a$10$HzzDEvxHYIg7IhmfGl.HjuJzT7uUB2tj1NKVeCkZhhmG9X.t4ajnu', null, 50000.00, false);
